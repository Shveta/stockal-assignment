import React from 'react';
import Header from '../components/header';

// Stateless Component working as Layout for whole web app
const SinglePageLayout = (props) => (
    <div className="stcl-page-wrapper">
        <Header />
        <props.children />
    </div>
)
export default SinglePageLayout
