import React from 'react'
import {
    BrowserRouter,
    Route,
    Switch,
    Link
} from 'react-router-dom'
import Home from '../home'
import SinglePageLayout from '../layouts/singlepagelayout'

const Routes = () => (
    <BrowserRouter basename={process.env.PUBLIC_URL}>
        <div>
            <Switch>
                <Route exact path="/"
                    render={(props) => (<SinglePageLayout children={Home} />)} />
                <Route exact path="/home"
                    render={(props) => (<SinglePageLayout children={Home} />)} />
                <Route exact path="/cart"
                    render={(props) => (<SinglePageLayout children={Home} />)} />
                <Route exact path="/products"
                    render={(props) => (<SinglePageLayout children={Home} />)} />
                {/******************** 404 Page-Route *********************/}
                <Route path="*" render={() => <div><h5 className="text-center"><span className="float-left">  <Link to='/home'>Go Back</Link></span>Not Found</h5></div>} />
            </Switch>
        </div>
    </BrowserRouter>
)

export default Routes