import React from 'react';
import axios from 'axios';
import OrderedProducts from './components/orderedproducts'
import Products from './components/products';
import CartProducts from './components/cartproducts';

//  Statefull Controller Component having knowledge of all possible states 
// and managing all logics(Adding/Removing to/from Cart) of this single page web app 
export default class Home extends React.Component {
    constructor() {
        super();
        // Initializing Predictable States of Stockal Assignment
        this.state = {
            data: [],
            selected_product: {},
            selected_product_sizes: [],
            txtQuantity: 0,
            ddlSize: '',
            cart_items: [],
            total_amount: 0
        }
    }

    componentDidMount = () => {
        this.GetProducts();
    }

    // Fetching Products from Server and Storing it into state
    GetProducts = async () => {
        let that = this;
        document.getElementById('stcl-product-div').setAttribute('class', 'row stcl-loader');
        await axios.get('http://images.stockal.com/api/products.json')
            .then(function (response) {
                document.getElementById('stcl-product-div').setAttribute('class', 'row');
                response = response.data;
                let result = [];
                result = response.success === true ? response.data.products : []
                that.setState({
                    data: result
                })
            })
            .catch(function (error) {
                console.log(error);
                document.getElementById('stcl-product-div').setAttribute('class', 'row');
            });
    }

    //***** Updating Selected Product when user select specific product from list
    HandleBuy = (product) => {
        this.setState({ selected_product: product, selected_product_sizes: product.sizes.split(',') })
    }

    //*****  Adding Products To Cart with validating it's size, qty and invetory
    AddToCart = () => {
        if (this.state.cart_items.length < 5) {
            if (this.state.txtQuantity > 0) {
                if (this.state.ddlSize !== '') {
                    let index = this.state.selected_product.inventoryInfo.findIndex(item => item.label === this.state.ddlSize)
                    if (index > -1) {
                        if (this.state.selected_product.inventoryInfo[index].inventory >= this.state.txtQuantity) {
                            let arr = this.state.cart_items;
                            let total_amount = this.state.total_amount + (this.state.selected_product.price * this.state.txtQuantity);
                            arr.push({ productId: this.state.selected_product.productId, productName: this.state.selected_product.productName, qty: this.state.txtQuantity, size: this.state.ddlSize, price: this.state.selected_product.price, thumb: this.state.selected_product.searchImage })
                            this.setState({ cart_items: arr, total_amount: total_amount })
                            alert("Product Added Successfully");
                            document.getElementById('btnCloseModal').click();
                        }
                        else
                            alert("Size Inventory not available");
                    }
                    else
                        alert("Inventory not available");
                }
                else
                    alert("Please Select the Size");
            }
            else
                alert("Please Select the Quantity");
        }
        else {
            alert("You can not add product more than 5 at a time. If you wish to add an item, remove any existing product from the order list.");
            document.getElementById('btnCloseModal').click();
        }
    }

    // ***** Handling OnChange Behaviour of Control Elements
    HandleOnChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    // ****** Closing Modal with updating selected Product
    CloseModal = () => {
        if (this.state.selected_product !== {})
            this.setState({ selected_product: {}, selected_product_sizes: [], txtQuantity: 0, ddlSize: '' })
    }

    //***** Checking if Product is already available in Cart
    CheckPreviouslyAdded = (product) => {
        if (this.state.cart_items.length > 0) {
            let index = this.state.cart_items.findIndex(item => item.productId === product.productId)
            if (index > -1)
                return <button type="button" className="btn btn-sm btn-outline-success disabled">Added</button>
            else
                return <button type="button" className="btn btn-sm btn-outline-info" data-toggle="modal" data-target="#myModal" onClick={() => this.HandleBuy(product)}>Buy</button>
        }
        else
            return <button type="button" className="btn btn-sm btn-outline-info" data-toggle="modal" data-target="#myModal" onClick={() => this.HandleBuy(product)}>Buy</button>
    }

    //****  Removing Products From Cart
    RemoveProduct = (productId) => {
        let index = this.state.cart_items.findIndex(item => item.productId === productId)
        if (index > -1) {
            let total_amount = this.state.cart_items[index].price * this.state.cart_items[index].qty;
            total_amount = this.state.total_amount - total_amount;
            let arr = this.state.cart_items;
            arr.splice(index, 1);
            this.setState({ cart_items: arr, total_amount: total_amount })
        }
    }

    render() {
        return (
            <div>
                <OrderedProducts cart_items={this.state.cart_items} RemoveProduct={this.RemoveProduct} total_amount={this.state.total_amount} />
                <Products data={this.state.data} CheckPreviouslyAdded={this.CheckPreviouslyAdded} />
                <CartProducts selected_product={this.state.selected_product}
                    selected_product_sizes={this.state.selected_product_sizes}
                    ddlSize={this.state.ddlSize}
                    txtQuantity={this.state.txtQuantity}
                    AddToCart={this.AddToCart}
                    HandleOnChange={this.HandleOnChange}
                    CloseModal={this.CloseModal}
                />
            </div >
        );
    }
}