import React from 'react';
import { Link } from 'react-router-dom'
//***** Stateless Header Component (Presentational Component) including all possible links
const Header = (props) => (
    <div>
        <nav className="navbar navbar-expand-sm bg-light navbar-light">
            <Link className="navbar-brand" to='/home'><img src={require('../assets/img/stockalblacklogo.png')} alt="logo" height="20" /></Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse justify-content-center" id="collapsibleNavbar">
                <ul className="navbar-nav">
                    <li className="nav-item text-center">
                        <Link className="nav-link" to='/home'>Home</Link>
                    </li>
                    <li className="nav-item text-center">
                        <Link className="nav-link" to='/products'>Products</Link>
                    </li>
                    <li className="nav-item text-center">
                        <Link className="nav-link" to='/cart'>Cart</Link>
                    </li>
                </ul>
            </div>
        </nav>
        <br />
    </div>
)
export default Header