import React from 'react';
//****** Component Shows Products List 
export default class Products extends React.Component {
  
    render() {
        return (
            <div className="container" id="stcl-product-section">
                <h4 className="text-center stcl-title"><b>Product List</b> </h4><br />
                <div className="row" id="stcl-product-div">
                    {this.props.data !== [] ? this.props.data.map(product =>
                        <div className="col-md-4 col-sm-6 col-xs-12" key={product.productId}>
                            <div className="row stcl-product-outer">
                                <div className="col-5">
                                    <img src={product.searchImage} className="stcl-product-img" alt="product" />
                                </div>
                                <div className="col-7">
                                    <div><b>Product Name : </b>{product.productName}</div>
                                    <div><b>Price : </b>{product.price}</div>
                                    <div><b>Brand : </b>{product.brand}</div>
                                    <div><b>Size : </b>{product.sizes !== '' ? product.sizes : "Not Available"}</div>
                                    <div className="text-center">
                                        {product.sizes !== '' ?
                                            this.props.CheckPreviouslyAdded(product)
                                            :
                                            <button type="button" className="btn btn-sm btn-outline-danger disabled stcl-not-in-stock">Out of Stock</button>
                                        }
                                    </div>
                                </div>
                            </div>

                        </div>
                    )
                        : <h5>No Products</h5>}
                </div>
            </div>
        )
    }
}