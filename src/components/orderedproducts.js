import React from 'react';
// ****** Component Shows Added Products in Cart
export default class OrderedProducts extends React.Component {

    render() {
        return (
            <div>
                {this.props.cart_items.length > 0 ?
                    <div className="container-fluid" id="stcl-cart-section">
						<div className="row">
                        <div className="col-4"></div>
                        <h6 className="text-center stcl-title col-md-4"><b>Ordered Product List</b></h6>
						<span className="float-right col-md-4">Total Amount : Rs. {this.props.total_amount}</span> 
						</div>
                        <div className="row">
                            {this.props.cart_items.map(item =>
                                <div key={item.productId} className="col stcl-product text-center">
                                    <div><img src={item.thumb} className="stcl-product-img" alt="product" /></div>
                                    <div><b>Product Name : </b>{item.productName}</div>
                                    <div><b>Price : </b>{item.price}</div>
                                    <div><b>No. of Items :</b>{item.qty}</div>
                                    <div><button type="button" className="btn btn-sm btn-outline-danger" onClick={() => this.props.RemoveProduct(item.productId)}>Remove</button></div>
                                </div>
                            )}
                        </div>
                    </div>
                    : <h5 className="text-center ">Your Cart is empty <img height="40" src={require('../assets/img/cart.png')} alt="cart" /></h5>}
                <hr />
            </div>
        )
    }
}