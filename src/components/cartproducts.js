import React from 'react';
//*****  Compoent Shows Selected Product going to be add in Cart
export default class CartProducts extends React.Component {
   
    render() {
        return (
            <div id="myModal" className="modal fade">
                <div className="modal-dialog">
                    {this.props.selected_product !== {} ?
                        < div className="modal-content">
                            <div className="modal-header" style={{ border: 'none', background: "#138496", color: '#ffffff' }}>
                                <h5 className="modal-title">Product : {this.props.selected_product.productName}</h5>
                                <button type="button" id="btnCloseModal" className="close" data-dismiss="modal" onClick={this.props.CloseModal}>&times;</button>
                            </div>

                            <div className="modal-body">
                                <div className="row justify-content-center">
                                    <img src={this.props.selected_product.searchImage} className="stcl-product-cart-img" alt="product" />
                                </div>
                                <br />
                                <div className="row justify-content-center">
                                    <div className="col-6 form-group">
                                        <div className="row">
                                            <label htmlFor="txtQuantity" className="col-6 text-center">Qty:</label>
                                            <input type="number" id="txtQuantity" name="txtQuantity" className="col-4" value={this.props.txtQuantity} placeholder="Quantity" onChange={this.props.HandleOnChange} style={{ width: 100, borderRadius: 500 }} />
                                        </div>
                                    </div>

                                    <div className="col-6 form-group">
                                        <div className="row">
                                            <label htmlFor="ddlSize" className="col-6 text-center">Size:</label>
                                            {this.props.selected_product_sizes !== '' ?
                                                <select id="ddlSize" name="ddlSize" value={this.props.ddlSize} onChange={this.props.HandleOnChange} className="col-4" style={{ height: 33, borderRadius: 500 }}>
                                                    <option value="-1">Size</option>
                                                    {this.props.selected_product_sizes.map((size,index) =>
                                                        <option key={"size_"+index} value={size}>{size}</option>
                                                    )}
                                                </select>
                                                : 'Size Not available'}
                                        </div>
                                    </div>
                                </div><br />
                                <div className="row justify-content-center">
                                    <button className="btn btn-info btn-block" onClick={this.props.AddToCart} style={{ width: '50%' }}>Buy</button>
                                </div>
                            </div>
                        </div>
                        : ''}
                </div>
            </div >
        )
    }
}